# WeatherCalculations Binding

This binding produces calculated meterological data based on raw observations from a weather station. Using temperature, humidity, pressure and wind speed, this binding is able to provide a range of of additional metrics:

- Wind Chill
- Heat Index
- Feels Like temperature
- Humidex
- Dew Point
- Sea Level Pressure
- Atmospheric Pressure Trend

## Supported Things

_Please describe the different supported things / devices within this section._
_Which different types are supported, which models were tested etc.?_
_Note that it is planned to generate some part of this based on the XML files within ```ESH-INF/thing``` of your binding._

## Discovery

This binding does not support auto-discovery. To create a new instance, go to the Inbox and click on "add new thing" and choose 
"Weather Calculations Binding".

## Thing Configuration

There are 5 required settings, and 1 optional setting. Values for the required settings must be provided before the Weather Calculations
thing will go online.

1. Elevation of station (Required)

This is the ground elevation (in meters above sea level) where the station is located.

2. Station height above ground (Required)

This is the height above the ground that the station is positioned.

3. Temperature item (Required)

This is the Item name or Channel ID of the item/channel that reports the temperature at your location. The temperature is assumed 
to be provided in degrees Celcius, or that the binding providing the value supports Units of Measurement (added in OpenHAB 2.3)

The Channel ID can be found in PaperUI by going to Configuration->Things->Your Weather Station. The Channel ID for each channel is listed underneath the channel description (Temperature, Pressure, etc). The exact value will vary depending on the type of weather station, but will look something like this:

stationtype:stationid:temperature

For example, for a Weatherflow SmartWeather station, the temperature might look like this:

weatherflowsmartweather:HB-00471:AR-01252:temperature


4. Humidity item (Required)

This is the Item name or Channel ID of the item/channel that reports the percent relative humidity at your location. 

See the previous section for instructions for finding the Channel ID to use for this value.

5. Pressure item (Required)

This is the Item name or Channel ID of the item/channel that reports the local atmospheric pressure at your location. Pressure is
assumed to be provided in millibar (MB) or hectopascals (hPa), or that the  binding providing the value supports Units of Measurement 
(added in OpenHAB 2.3.)

See the "Temperature item" section for instructions for finding the Channel ID to use for this value.

6. Wind speed item (Optional)

This is the Item name or Channel ID of the item/channel that reports the wind speed at your location. This item is optional, and if present,
will be used to calculate wind chill values. Wind speed is assumed to be provided in meters/second (m/s), or that the binding providing the
value supports Units of Measurement (added in OpenHAB 2.3.)

See the "Temperature item" section for instructions for finding the Channel ID to use for this value.

## Channels

_Here you should provide information about available channel types, what their meaning is and how they can be used._

_Note that it is planned to generate some part of this based on the XML files within ```ESH-INF/thing``` of your binding._
