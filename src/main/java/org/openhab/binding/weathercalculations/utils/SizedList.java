package org.openhab.binding.weathercalculations.utils;

import org.joda.time.DateTime;

import java.util.LinkedList;

public class SizedList extends LinkedList<PressureValue> {

    private int maxSize;

    public SizedList(int size) {
        super();
        this.maxSize = size;
    }

    @Override
    public boolean add(PressureValue o) {

        //If the stack is too big, remove elements until it's the right size.
        while (this.size() >= maxSize) {
            this.remove(0);
        }
        return super.add(o);
    }

    @Override
    public String toString() {
        return "SizedList{" + genelems() + "}";
    }

    private String genelems() {
        String s = "";

        for(PressureValue pv : this) {
            s += "PV(" + new DateTime(pv.getStored()) + ": " + pv.getValue() + ") ";
        }

        return s;
    }
}
