package org.openhab.binding.weathercalculations.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.TimeZone;

/**
 * This class holds various unit/measurement conversion methods
 *
 * @author Gaël L'hopital
 * @author William Welliver
 */
public class WeatherUtils {

    private static final long FIFTEEN_MINUTES = (15 * 60 * 1000);
    public static SizedList pressureTrendStore = new SizedList(5);
    private static Logger log = LoggerFactory.getLogger(WeatherUtils.class);

    /**
     * Utility method to calculate the heat index, given a Celsius temperature (C) and relative humidity (rh).
     *
     * See http://www.srh.noaa.gov/images/ffc/pdf/ta_htindx.PDF for calculation details, noting that there is a
     * degree of variance in result to be expected since the equation is an approximation derived from regression
     * analysis.
     *
     * @param temp in (°C)
     * @param rh relative level (%)
     */
    public static double getHeatIndex(double temp, double rh) {
        double hi;

        if (temp <= 4.44) {
            hi = temp;
        } else {
            double tempair_in_fahrenheit = (1.80 * temp) + 32.0;
            double hitemp = 61.0 + ((tempair_in_fahrenheit - 68.0) * 1.2) + (rh * 0.094);
            double fptemp = temp;
            double hifinal = 0.5 * (fptemp + hitemp);

            if (hifinal > 79.0) {
                hi = -42.379 + 2.04901523 * tempair_in_fahrenheit + 10.14333127 * rh
                        - 0.22475541 * tempair_in_fahrenheit * rh
                        - 6.83783 * (Math.pow(10, -3)) * (Math.pow(tempair_in_fahrenheit, 2))
                        - 5.481717 * (Math.pow(10, -2)) * (Math.pow(rh, 2))
                        + 1.22874 * (Math.pow(10, -3)) * (Math.pow(tempair_in_fahrenheit, 2)) * rh
                        + 8.5282 * (Math.pow(10, -4)) * tempair_in_fahrenheit * (Math.pow(rh, 2))
                        - 1.99 * (Math.pow(10, -6)) * (Math.pow(tempair_in_fahrenheit, 2)) * (Math.pow(rh, 2));

                if ((rh <= 13) && (tempair_in_fahrenheit >= 80.0) && (tempair_in_fahrenheit <= 112.0)) {
                    double adj1 = (13.0 - rh) / 4.0;
                    double adj2 = Math.sqrt((17.0 - Math.abs(tempair_in_fahrenheit - 95.0)) / 17.0);
                    double adj = adj1 * adj2;
                    hi = hi - adj;
                } else if ((rh > 85.0) && (tempair_in_fahrenheit >= 80.0) && (tempair_in_fahrenheit <= 87.0)) {
                    double adj1 = (rh - 85.0) / 10.0;
                    double adj2 = (87.0 - tempair_in_fahrenheit) / 5.0;
                    double adj = adj1 * adj2;
                    hi = hi + adj;
                }
            } else {
                hi = hifinal;
            }

            hi = (hi - 32) * 0.9; // back to C
        }

        BigDecimal a = new BigDecimal(hi);
        return a.setScale(0, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    // public static double getHeatIndex(double temperature, double humidity) {
    // double t = (9 / 5) * temperature + 32; // switch to °F
    // double hi = 16.923 + (1.85212 * Math.pow(10, -1) * t) + (5.37941 * humidity)
    // - (1.00254 * Math.pow(10, -1) * t * humidity) + (9.41695 * Math.pow(10, -3) * Math.pow(t, 2))
    // + (7.28898 * Math.pow(10, -3) * Math.pow(humidity, 2))
    // + (3.45372 * Math.pow(10, -4) * Math.pow(t, 2) * humidity)
    // - (8.14971 * Math.pow(10, -4) * t * Math.pow(humidity, 2))
    // + (1.02102 * Math.pow(10, -5) * Math.pow(t, 2) * Math.pow(humidity, 2))
    // - (3.8646 * Math.pow(10, -5) * Math.pow(t, 3)) + (2.91583 * Math.pow(10, -5) * Math.pow(humidity, 3))
    // + (1.42721 * Math.pow(10, -6) * Math.pow(t, 3) * humidity)
    // + (1.97483 * Math.pow(10, -7) * t * Math.pow(humidity, 3))
    // - (2.18429 * Math.pow(10, -8) * Math.pow(t, 3) * Math.pow(humidity, 2))
    // + (8.43296 * Math.pow(10, -10) * Math.pow(t, 2) * Math.pow(humidity, 3))
    // - (4.81975 * Math.pow(10, -11) * Math.pow(t, 3) * Math.pow(humidity, 3));
    // hi = (5.0 / 9.0) * (hi - 32); // get back to °C
    // return hi;
    // }

    public static double getDewPointDep(double temperature, double dewpoint) {
        return temperature - dewpoint;
    }

    /**
     * Compute the Dewpoint temperature given temperature and hygrometry
     * valid up to 60 degrees, from
     * http://en.wikipedia.org/wiki/Dew_point#Calculating_the_dew_point
     *
     * @param temperature in (°C)
     * @param humidity relative level (%)
     * @return dewpoint temperature
     */
    public static double getDewPoint(double temperature, double humidity) {
        double a = 17.271, b = 237.2;
        double gamma = ((a * temperature) / (b + temperature)) + Math.log(humidity / 100.0);
        return b * gamma / (a - gamma);
    }

    /**
     * Compute the Humidex index given temperature and hygrometry
     *
     *
     * @param temperature in (°C)
     * @param hygro relative level (%)
     * @return Humidex index value
     */
    public static double getHumidex(double temperature, double hygro) {
        double result = 6.112 * Math.pow(10, 7.5 * temperature / (237.7 + temperature)) * hygro / 100;
        result = temperature + 0.555555556 * (result - 10);
        return result;
    }

    /**
     * Compute the feels like temperature.
     *
     * @param temperature in (°C)
     * @param humidity relative level (%)
     * @param windspeed in meters per second (mps)
     * @return
     */
    public static double getFeelsLike(double temperature, double humidity, double windspeed) {
        if (temperature > 26.6667) {
            return getHeatIndex(temperature, humidity);
        } else if (temperature < 10.0) {
            return getWindChill(temperature, windspeed);
        } else {
            return temperature;
        }
    }

    /**
     * Compute the wind chill temperature
     *
     * @param temperature in (°C)
     * @param windspeed in meters per second (mps)
     * @return
     */
    public static double getWindChill(double temperature, double windspeed) {
        double t = (9 / 5) * temperature + 32; // switch to °F
        double w = ((windspeed * 3600) * 3.2808) / 5280.0; // switch to mph

        if ((t > 50.0 || w < 3.0)) {
            return temperature;
        }

        double wc = 35.74 + (0.6215 * t) - (35.75 * Math.pow(w, 0.16)) + (0.4275 * temperature * Math.pow(w, 0.16));

        double windchill = (5.0 / 9.0) * (wc - 32); // get back to °C

        return windchill;
    }

    /**
     * Compute atmospheric pressure at sea level
     *
     * @param pressure in mbar
     * @param temperature in degrees celcius
     * @param elevation in meters
     * @param stationElevation in meters above ground
     * @return
     */
    public static double getSeaLevelPressure(double pressure, double temperature, double elevation, double stationElevation) {
        return calcSeaPressure(pressure, elevation + stationElevation, temperature);
    }

    private static double calcSeaPressure(double pressure, double altitude, double temp) {
        double temperature = temp + 273.15;
        double tempGradient = 0.0065;

        double sealevelPressure = (pressure) *  Math.pow(1 - ((altitude * tempGradient) / (temperature + (altitude * tempGradient)) ),  -5.257);
        //sealevelPressure = Math.round(sealevelPressure * 100) / 100;
        return sealevelPressure;
    }

    public static int calculetePressureTrend(double pressure, long measurementTime) {

        // if the pressure store is empty or the newest entry is more than 15 minutes old, add the entry.
        long last;

        measurementTime = roundDown(measurementTime);

        if(pressureTrendStore.isEmpty())
            while(pressureTrendStore.size() < 5)
                pressureTrendStore.add(new PressureValue(pressure, measurementTime));

        last = pressureTrendStore.get(4).getStored();

        last += FIFTEEN_MINUTES; // 15 minutes;

        while (last <= measurementTime) {
            pressureTrendStore.add(new PressureValue(pressure, measurementTime));
            last += FIFTEEN_MINUTES;
        }


        double diff = pressureTrendStore.get(4).getValue() - pressureTrendStore.get(3).getValue();

        log.debug("Pressure history: " + pressureTrendStore);
        log.debug("Pressure diff: " + diff);

        if(diff >= 2.0/4) return 2; // rising rapidly;
        else if(diff > 1.0/4) return 1; // rising slowly;
        else if(diff <= -2.0/4) return -2; // dropping rapidly;
        else if(diff < -1.0/4) return -1; // dropping slowly;
        return 0;
    }

    private static long roundDown(long measurementTime) {
        LocalDateTime time = LocalDateTime.ofInstant(Instant.ofEpochMilli(measurementTime),
                TimeZone.getDefault().toZoneId());
        LocalDateTime lastQuarter = time.truncatedTo(ChronoUnit.HOURS)
                .plusMinutes(15 * (time.getMinute() / 15));

        return lastQuarter.atZone(TimeZone.getDefault().toZoneId()).toEpochSecond() *  1000;
    }

}
