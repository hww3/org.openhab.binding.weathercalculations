package org.openhab.binding.weathercalculations.utils;

public class PressureValue {
    private long stored;
    private double value;

    public PressureValue(double pressure, long measurementTime) {
        value = pressure; stored = measurementTime;
    }

    public long getStored() {
        return stored;
    }

    public void setStored(long stored) {
        this.stored = stored;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
