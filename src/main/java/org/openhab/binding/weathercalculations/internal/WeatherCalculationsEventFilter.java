package org.openhab.binding.weathercalculations.internal;

import java.util.Collection;
import java.util.HashSet;

import org.eclipse.smarthome.core.events.Event;
import org.eclipse.smarthome.core.events.EventFilter;
import org.eclipse.smarthome.core.items.events.ItemStateChangedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WeatherCalculationsEventFilter implements EventFilter {

    private Collection<String> items = new HashSet<>();
    private final Logger logger = LoggerFactory.getLogger(WeatherCalculationsEventFilter.class);

    public void addItems(Collection<String> items) {
        for(String item : items)
          this.items.add(item.replace(":", "_").replace("-", "_"));
    }

    public void removeItems(Collection<String> items) {
        for(String item : items)
          this.items.remove(item.replace(":", "_").replace("-", "_"));
    }

    public void clearItems() {
        this.items.clear();
    }

    @Override
    public boolean apply(Event event) {
        if (event instanceof ItemStateChangedEvent) {
            ItemStateChangedEvent e = (ItemStateChangedEvent) event;
            //logger.debug("Receive Update: " + e + ", " + e.getItemName()
            //    + ", " + e.getType() + ", items: " + items.size());

            if (items.contains(e.getItemName())) {
                return true;
            }
        }
        return false;
    }
}
