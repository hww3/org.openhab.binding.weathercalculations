package org.openhab.binding.weathercalculations.internal;

import org.eclipse.smarthome.config.core.*;
import org.eclipse.smarthome.core.items.GenericItem;
import org.eclipse.smarthome.core.items.Item;
import org.eclipse.smarthome.core.items.ItemRegistry;
import org.eclipse.smarthome.core.library.items.NumberItem;
import org.eclipse.smarthome.core.thing.*;
import org.eclipse.smarthome.core.thing.type.ThingType;
import org.eclipse.smarthome.core.thing.type.ThingTypeRegistry;
import org.openhab.binding.weathercalculations.WeatherCalculationsBindingConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.*;

public class WeatherCalculationConfigProvider  implements ConfigDescriptionProvider, ConfigOptionProvider {
    private final static Logger logger = LoggerFactory.getLogger(WeatherCalculationConfigProvider.class);

    private static ThingRegistry thingRegistry;
    private static ItemRegistry itemRegistry;
    private static ThingTypeRegistry thingTypeRegistry;
    private static ConfigDescriptionRegistry configDescriptionRegistry;

    private static Set<ThingTypeUID> zwaveThingTypeUIDList = new HashSet<ThingTypeUID>();

    private static final Object productIndexLock = new Object();

    protected void setThingRegistry(ThingRegistry thingRegistry) {
        WeatherCalculationConfigProvider.thingRegistry = thingRegistry;
    }

    protected void unsetThingRegistry(ThingRegistry thingRegistry) {
        WeatherCalculationConfigProvider.thingRegistry = null;
    }

    protected void setItemRegistry(ItemRegistry itemRegistry) {
        WeatherCalculationConfigProvider.itemRegistry = itemRegistry;
    }

    protected void unsetItemRegistry(ItemRegistry itemRegistry) {
        WeatherCalculationConfigProvider.itemRegistry = null;
    }

    protected void setThingTypeRegistry(ThingTypeRegistry thingTypeRegistry) {
        WeatherCalculationConfigProvider.thingTypeRegistry = thingTypeRegistry;
    }

    protected void unsetThingTypeRegistry(ThingTypeRegistry thingTypeRegistry) {
        WeatherCalculationConfigProvider.thingTypeRegistry = null;
    }

    protected void setConfigDescriptionRegistry(ConfigDescriptionRegistry configDescriptionRegistry) {
        WeatherCalculationConfigProvider.configDescriptionRegistry = configDescriptionRegistry;
    }

    protected void unsetConfigDescriptionRegistry(ConfigDescriptionRegistry configDescriptionRegistry) {
        WeatherCalculationConfigProvider.configDescriptionRegistry = null;
    }

    @Override
    public Collection<ConfigDescription> getConfigDescriptions(Locale locale) {
        logger.debug("getConfigDescriptions called");
        return Collections.emptySet();
    }

    @Override
    public ConfigDescription getConfigDescription(URI uri, Locale locale) {
        if (uri == null) {
            return null;
        }

        if ("thing".equals(uri.getScheme()) == false) {
            return null;
        }

        ThingUID thingUID = new ThingUID(uri.getSchemeSpecificPart());
        ThingType thingType = thingTypeRegistry.getThingType(thingUID.getThingTypeUID());
        if (thingType == null) {
            return null;
        }

        // Is this a zwave thing?
        if (!thingUID.getBindingId().equals(WeatherCalculationsBindingConstants.BINDING_ID)) {
            return null;
        }

        Thing thing = getThing(thingUID);
        if (thing == null) {
            return null;
        }

        List<ConfigDescriptionParameterGroup> groups = new ArrayList<ConfigDescriptionParameterGroup>();
        List<ConfigDescriptionParameter> parameters = new ArrayList<ConfigDescriptionParameter>();

        groups.add(new ConfigDescriptionParameterGroup("datasourcecfg", "home", false, "Data Source Configuration", ""));

        List<ParameterOption> options = new ArrayList<ParameterOption>();

        Set<String> categories = new HashSet<>();
        categories.add("Temperature");
        categories.add("Pressure");
        categories.add("Humidity");
        categories.add("Wind");

        for(GenericItem it: itemRegistry.getItemsByTag(NumberItem.class)) {
            String category = it.getCategory();
            if(category == null || categories.contains(category))
            options.add(new ParameterOption(it.getName(), it.getLabel() + " (" + it.getName() + ")"));
        }

//        for(Thing th : thingRegistry.getAll()) {
//            for(Channel ch : th.getChannels()) {
//                options.add(new ParameterOption(ch.getUID().getAsString(), th.getLabel() + " / " + ch.getLabel()));
//            }
//        }

        parameters.add(ConfigDescriptionParameterBuilder
                .create(WeatherCalculationsBindingConstants.CONFIG_TEMPERATURE_ITEM, ConfigDescriptionParameter.Type.TEXT)
                .withLabel(
                        WeatherCalculationsBindingConstants.getI18nConstant(WeatherCalculationsBindingConstants.CONFIG_TEMPERATURE_ITEM_LABEL))
                .withDescription(
                        WeatherCalculationsBindingConstants.getI18nConstant(WeatherCalculationsBindingConstants.CONFIG_TEMPERATURE_ITEM_DESC))
                .withOptions(options).withRequired(true).withLimitToOptions(false).withGroupName("datasourcecfg").build());

        parameters.add(ConfigDescriptionParameterBuilder
                .create(WeatherCalculationsBindingConstants.CONFIG_PRESSURE_ITEM, ConfigDescriptionParameter.Type.TEXT)
                .withLabel(
                        WeatherCalculationsBindingConstants.getI18nConstant(WeatherCalculationsBindingConstants.CONFIG_PRESSURE_ITEM_LABEL))
                .withDescription(
                        WeatherCalculationsBindingConstants.getI18nConstant(WeatherCalculationsBindingConstants.CONFIG_PRESSURE_ITEM_DESC))
                .withOptions(options).withRequired(true).withLimitToOptions(false).withGroupName("datasourcecfg").build());

        parameters.add(ConfigDescriptionParameterBuilder
                .create(WeatherCalculationsBindingConstants.CONFIG_HUMIDITY_ITEM, ConfigDescriptionParameter.Type.TEXT)
                .withLabel(
                        WeatherCalculationsBindingConstants.getI18nConstant(WeatherCalculationsBindingConstants.CONFIG_HUMIDITY_ITEM_LABEL))
                .withDescription(
                        WeatherCalculationsBindingConstants.getI18nConstant(WeatherCalculationsBindingConstants.CONFIG_HUMIDITY_ITEM_DESC))
                .withOptions(options).withRequired(true).withLimitToOptions(false).withGroupName("datasourcecfg").build());

        parameters.add(ConfigDescriptionParameterBuilder
                .create(WeatherCalculationsBindingConstants.CONFIG_WIND_SPEED_ITEM, ConfigDescriptionParameter.Type.TEXT)
                .withLabel(
                        WeatherCalculationsBindingConstants.getI18nConstant(WeatherCalculationsBindingConstants.CONFIG_WIND_SPEED_ITEM_LABEL))
                .withDescription(
                        WeatherCalculationsBindingConstants.getI18nConstant(WeatherCalculationsBindingConstants.CONFIG_WIND_SPEED_ITEM_DESC))
                .withOptions(options).withRequired(false).withLimitToOptions(false).withGroupName("datasourcecfg").build());

        return new ConfigDescription(uri, parameters, groups);
    }

    @Override
    public Collection<ParameterOption> getParameterOptions(URI uri, String param, Locale locale) {
        // We need to update the options of all requests for association groups...
        if (!"thing".equals(uri.getScheme())) {
            return null;
        }

        ThingUID thingUID = new ThingUID(uri.getSchemeSpecificPart());
        ThingType thingType = thingTypeRegistry.getThingType(thingUID.getThingTypeUID());
        if (thingType == null) {
            return null;
        }

        return null;
    }

    public static Thing getThing(ThingUID thingUID) {
        // Check that we know about the registry
        if (thingRegistry == null) {
            return null;
        }

        return thingRegistry.get(thingUID);
    }
}