package org.openhab.binding.weathercalculations.internal;

import java.util.Collection;

import org.eclipse.smarthome.core.events.AbstractTypedEventSubscriber;
import org.eclipse.smarthome.core.events.EventFilter;
import org.eclipse.smarthome.core.events.EventSubscriber;
import org.eclipse.smarthome.core.items.events.ItemStateChangedEvent;
import org.openhab.binding.weathercalculations.handler.WeatherCalculationsHandler;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = { EventSubscriber.class,
        WeatherCalculatorEventSubscriber.class }, configurationPid = "binding.weathercalculations")
public class WeatherCalculationsEventSubscriberImpl extends AbstractTypedEventSubscriber<ItemStateChangedEvent>
        implements WeatherCalculatorEventSubscriber {

    private final Logger logger = LoggerFactory.getLogger(WeatherCalculationsHandler.class);

    WeatherCalculationsEventFilter filter = new WeatherCalculationsEventFilter();

    private ItemStateChangedEventListener listener;

    public WeatherCalculationsEventSubscriberImpl() {
        super(ItemStateChangedEvent.TYPE);
    }

    @Override
    public EventFilter getEventFilter() {
        return filter;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.openhab.binding.weathercalculations.internal.IWeatherCalculatorEventSubscriber#addItems(java.util.Collection)
     */
    @Override
    public void addItems(Collection<String> items) {
        filter.addItems(items);
        //logger.info("Added items to event filter: " + items);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.openhab.binding.weathercalculations.internal.IWeatherCalculatorEventSubscriber#removeItems(java.util.
     * Collection)
     */
    @Override
    public void removeItems(Collection<String> items) {
        filter.removeItems(items);
    }


    @Override
    public void clearItems() { filter.clearItems(); }

    /*
     * (non-Javadoc)
     *
     * @see org.openhab.binding.weathercalculations.internal.IWeatherCalculatorEventSubscriber#setListener(org.openhab.
     * binding.weathercalculations.internal.ItemStateChangedEventListener)
     */
    @Override
    public void setListener(ItemStateChangedEventListener listener) {
        this.listener = listener;
    }

    @Override
    protected void receiveTypedEvent(ItemStateChangedEvent updateEvent) {
         //logger.info("Receive Update: " + updateEvent + ", " + updateEvent.getItemName() + ", " +
         //updateEvent.getType());
        if (listener != null) {
            listener.eventReceived(updateEvent);
        }
    }

}
