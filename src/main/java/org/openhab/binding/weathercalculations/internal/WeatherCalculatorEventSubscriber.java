package org.openhab.binding.weathercalculations.internal;

import java.util.Collection;

public interface WeatherCalculatorEventSubscriber {

    void addItems(Collection<String> items);

    void removeItems(Collection<String> items);

    void clearItems();

    void setListener(ItemStateChangedEventListener listener);

}