package org.openhab.binding.weathercalculations.internal;

import org.eclipse.smarthome.core.items.events.ItemStateChangedEvent;

public interface ItemStateChangedEventListener {
    public void eventReceived(ItemStateChangedEvent event);
}
