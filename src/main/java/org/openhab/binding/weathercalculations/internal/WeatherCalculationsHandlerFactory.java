/**
 * Copyright (c) 2014,2018 by the respective copyright holders.
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.weathercalculations.internal;

import static org.openhab.binding.weathercalculations.WeatherCalculationsBindingConstants.THING_TYPE_WEATHER_CALCULATIONS;

import java.util.Collections;
import java.util.Set;

import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandlerFactory;
import org.eclipse.smarthome.core.thing.binding.ThingHandler;
import org.eclipse.smarthome.core.thing.binding.ThingHandlerFactory;
import org.openhab.binding.weathercalculations.WeatherCalculationsBindingConstants;
import org.openhab.binding.weathercalculations.handler.WeatherCalculationsHandler;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link WeatherCalculationsHandlerFactory} is responsible for creating things and thing
 * handlers.
 *
 * @author William Welliver - Initial contribution
 */
@Component(service = ThingHandlerFactory.class, immediate = true, configurationPid = "binding.weathercalculations")
public class WeatherCalculationsHandlerFactory extends BaseThingHandlerFactory {
    private final Logger logger = LoggerFactory.getLogger(WeatherCalculationsHandlerFactory.class);
    protected WeatherCalculatorEventSubscriber subscriber;
    protected static BundleContext bundleContext;
    private static final Set<ThingTypeUID> SUPPORTED_THING_TYPES_UIDS = Collections
            .singleton(THING_TYPE_WEATHER_CALCULATIONS);

    public WeatherCalculationsHandlerFactory() {
        super();
        WeatherCalculationsBindingConstants.setHandlerFactory(this);
    }

    public Bundle getBundle() {
        return getBundleContext().getBundle();
    }

    @Override
    public boolean supportsThingType(ThingTypeUID thingTypeUID) {
        return SUPPORTED_THING_TYPES_UIDS.contains(thingTypeUID);
    }

    @Override
    protected @Nullable ThingHandler createHandler(Thing thing) {
        ThingTypeUID thingTypeUID = thing.getThingTypeUID();
        if (thingTypeUID.equals(THING_TYPE_WEATHER_CALCULATIONS)) {
            return new WeatherCalculationsHandler(thing, subscriber);
        }

        return null;
    }

    @Reference()
    public void bindEventSubscriber(WeatherCalculatorEventSubscriber subscriber) {

        logger.info("bindEventSubscriber: " + subscriber);
        this.subscriber = subscriber;
    }

    public void unbindEventSubscriber(WeatherCalculatorEventSubscriber subscriber) {

        logger.info("unbindEventSubscriber: " + subscriber);
        this.subscriber = null;
    }
}
