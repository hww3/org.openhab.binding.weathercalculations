/**
 * Copyright (c) 2014,2018 by the respective copyright holders.
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.weathercalculations.handler;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.smarthome.core.items.events.ItemStateChangedEvent;
import org.eclipse.smarthome.core.library.types.DecimalType;
import org.eclipse.smarthome.core.library.types.QuantityType;
import org.eclipse.smarthome.core.library.types.StringType;
import org.eclipse.smarthome.core.library.unit.SIUnits;
import org.eclipse.smarthome.core.library.unit.SmartHomeUnits;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandler;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.State;
import org.openhab.binding.weathercalculations.WeatherCalculationsBindingConstants;
import org.openhab.binding.weathercalculations.internal.ItemStateChangedEventListener;
import org.openhab.binding.weathercalculations.internal.WeatherCalculatorEventSubscriber;
import org.openhab.binding.weathercalculations.utils.WeatherUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.measure.Quantity;
import javax.measure.Unit;
import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Temperature;

import static org.eclipse.smarthome.core.library.unit.MetricPrefix.HECTO;

/**
 * The {@link WeatherCalculationsHandler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author William Welliver - Initial contribution
 */
public class WeatherCalculationsHandler extends BaseThingHandler implements ItemStateChangedEventListener {

    private final Logger logger = LoggerFactory.getLogger(WeatherCalculationsHandler.class);

    private final WeatherCalculatorEventSubscriber subscriber;

    private String temperatureItem;
    private String pressureItem;
    private String humidityItem;
    private String windSpeedItem;

    private Double temperature = null;
    private Double pressure = null;
    private Double humidity = null;
    private Double windSpeed = null;

    private BigDecimal elevation = null;
    private BigDecimal stationElevation = null;

    public WeatherCalculationsHandler(Thing thing, WeatherCalculatorEventSubscriber subscriber) {
        super(thing);
        this.subscriber = subscriber;
    }

    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
    }

    @Override
    public void initialize() {
        Set<String> items = new HashSet<>();

        Map<String, Object> config = getThing().getConfiguration().getProperties();
        logger.info("config:" + config);
        temperatureItem = (String) config.get(WeatherCalculationsBindingConstants.CONFIG_TEMPERATURE_ITEM);
        pressureItem = (String) config.get("pressureItem");
        humidityItem = (String) config.get("humidityItem");
        windSpeedItem = (String) config.get("windSpeedItem");

        elevation = (BigDecimal) (config.get("altitude"));
        stationElevation = (BigDecimal) (config.get("stationAltitude"));

        items.add(temperatureItem);
        items.add(pressureItem);
        items.add(humidityItem);

        if (windSpeedItem != null && !windSpeedItem.isEmpty()) {
            items.add(windSpeedItem);
        } else {
            windSpeed = 0.0;
        }

        // TODO need to be able to manage possibility of multiple instances of this binding wrt the listener
        subscriber.clearItems();
        subscriber.addItems(items);
        subscriber.setListener(this);
        // subscriber.removeItems(items);

        updateStatus(ThingStatus.ONLINE);
        temperatureItem = temperatureItem.replace(":", "_").replace("-", "_");
        humidityItem = humidityItem.replace(":", "_").replace("-", "_");
        pressureItem = pressureItem.replace(":", "_").replace("-", "_");
        if(windSpeedItem != null)
           windSpeedItem = windSpeedItem.replace(":", "_").replace("-", "_");
    }

    protected double toUnit(State val, Unit u) {
        double oVal;

        if(val instanceof QuantityType) {
            QuantityType quantityType = (QuantityType)val;
            return quantityType.toUnit(u).doubleValue();
        } else if(val instanceof DecimalType) {
            QuantityType quantityType;
            quantityType = new QuantityType(((DecimalType) val).doubleValue(), u);
            return quantityType.toUnit(u).doubleValue();
        } else {
            logger.warn("Expected value of type DecimalType or QuantityType, received " + val.getClass().getName() + ": " + val);

            // I will surely go to hell for this, but it's the easiest solution to the problem.
            throw new IllegalArgumentException("Illegal argument type");
        }
    }

    @Override
    public void eventReceived(ItemStateChangedEvent event) {
        String itemName = event.getItemName();
        State s = event.getItemState();
        boolean temperatureChanged = false, pressureChanged = false, humidityChanged = false, windSpeedChanged = false;
        logger.debug("Item " + itemName + " changed to " + s);

        if(s == null) { 
            logger.warn("Got null state from event " + event);
            return;
        }

        try {
            if (itemName.equals(temperatureItem)) {
                temperature = toUnit(s, SIUnits.CELSIUS);
                temperatureChanged = true;
            } else if (itemName.equals(pressureItem)) {
                pressure = toUnit(s, HECTO(SIUnits.PASCAL));
                pressureChanged = true;
            } else if (itemName.equals(humidityItem)) {
                humidity = toUnit(s, SmartHomeUnits.PERCENT);
                humidityChanged = true;
            } else if (itemName.equals(windSpeedItem)) {
                windSpeed = toUnit(s, SmartHomeUnits.METRE_PER_SECOND);
                windSpeedChanged = true;
            } else {
                logger.warn(
                        "Got an event for an item we're not interested in: " + itemName + ". Perhaps a bug lurks here.");
            }
        } catch (IllegalArgumentException ex) {
            return;
        }

        // now, we must decide what to recalculate based on what's changed and what we have data for already.

        if ((pressureChanged || temperatureChanged) && (pressure != null && temperature != null)) {
            double slp = WeatherUtils.getSeaLevelPressure(pressure.doubleValue(), temperature.doubleValue(), elevation.doubleValue(),
                    stationElevation.doubleValue());
            logger.debug("Sea Level Pressure: " + slp);
            this.updateState(WeatherCalculationsBindingConstants.CHANNEL_SEA_LEVEL_PRESSURE, new QuantityType<>(slp, HECTO(SIUnits.PASCAL)));

            // Note that we assume that measurements are arriving in approximately real time, though that's not necessarily the case.
            int roc = WeatherUtils.calculetePressureTrend(pressure.doubleValue(), System.currentTimeMillis());
            logger.debug("Pressure trend: " + roc);
            this.updateState(WeatherCalculationsBindingConstants.CHANNEL_PRESSURE_TREND, new StringType("" + roc));
        }

        if ((temperatureChanged || humidityChanged) && (temperature != null && humidity != null)) {
            this.updateState(WeatherCalculationsBindingConstants.CHANNEL_HUMIDEX,
                    new QuantityType<>(WeatherUtils.getHumidex(temperature.doubleValue(), humidity.doubleValue()), SmartHomeUnits.ONE));
            this.updateState(WeatherCalculationsBindingConstants.CHANNEL_DEW_POINT,
                    new QuantityType<>(WeatherUtils.getDewPoint(temperature.doubleValue(), humidity.doubleValue()), SIUnits.CELSIUS));
            this.updateState(WeatherCalculationsBindingConstants.CHANNEL_HEAT_INDEX,
                    new QuantityType<>(WeatherUtils.getHeatIndex(temperature.doubleValue(), humidity.doubleValue()), SIUnits.CELSIUS));
        }

        if ((temperatureChanged || windSpeedChanged) && (temperature != null && windSpeed != null)) {
            this.updateState(WeatherCalculationsBindingConstants.CHANNEL_WIND_CHILL,
                    new QuantityType<>(WeatherUtils.getWindChill(temperature.doubleValue(), windSpeed.doubleValue()), SIUnits.CELSIUS));
        }

        if ((temperatureChanged || windSpeedChanged || humidityChanged)
                && (temperature != null && windSpeed != null && humidity != null)) {
            double flt = WeatherUtils.getFeelsLike(temperature.doubleValue(), humidity.doubleValue(),
                    windSpeed.doubleValue());
            logger.debug("Feels like: " + flt);
            this.updateState(WeatherCalculationsBindingConstants.CHANNEL_FEELS_LIKE, new QuantityType<Temperature>(flt, SIUnits.CELSIUS));
        }
    }

    protected <U extends Quantity<U>> QuantityType<U> commandToQuantityType(Command command, Unit<U> defaultUnit) {
        if (command instanceof QuantityType) {
            return (QuantityType<U>) command;
        }
        return new QuantityType<U>(new BigDecimal(command.toString()), defaultUnit);
    }
}
