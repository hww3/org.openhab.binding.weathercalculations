/**
 * Copyright (c) 2014,2018 by the respective copyright holders.
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.weathercalculations;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.smarthome.core.i18n.TranslationProvider;
import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.openhab.binding.weathercalculations.internal.WeatherCalculationsHandlerFactory;
import org.osgi.framework.BundleContext;

import java.text.MessageFormat;

/**
 * The {@link WeatherCalculationsBindingConstants} class defines common constants, which are
 * used across the whole binding.
 *
 * @author William Welliver - Initial contribution
 */
@NonNullByDefault
public class WeatherCalculationsBindingConstants {

    public static final String BINDING_ID = "weathercalculations";

    // List of all Thing Type UIDs
    public static final ThingTypeUID THING_TYPE_WEATHER_CALCULATIONS = new ThingTypeUID(BINDING_ID,
            "weathercalculations");

    // List of all Channel ids
    public static final String CHANNEL_SEA_LEVEL_PRESSURE = "seaLevelPressure";
    public static final String CHANNEL_PRESSURE_TREND = "pressureTrend";
    public static final String CHANNEL_HUMIDEX = "humidex";
    public static final String CHANNEL_DEW_POINT = "dewPoint";
    public static final String CHANNEL_HEAT_INDEX = "heatIndex";
    public static final String CHANNEL_WIND_CHILL = "windChill";
    public static final String CHANNEL_FEELS_LIKE = "feelsLike";
    
    public static final String CONFIG_TEMPERATURE_ITEM = "temperatureItem";
    public static final I18nConstant CONFIG_TEMPERATURE_ITEM_LABEL = new I18nConstant("thing-type.config.weathercalculations.weathercalculations.temperature_label","Temperature Item");
    public static final I18nConstant CONFIG_TEMPERATURE_ITEM_DESC = new I18nConstant("thing-type.config.weathercalculations.weathercalculations.temperature_desc","Item that provides the current temperature in ºC");

    public static final String CONFIG_PRESSURE_ITEM = "pressureItem";
    public static final I18nConstant CONFIG_PRESSURE_ITEM_LABEL = new I18nConstant("thing-type.config.weathercalculations.weathercalculations.pressure_label","Pressure Item");
    public static final I18nConstant CONFIG_PRESSURE_ITEM_DESC = new I18nConstant("thing-type.config.weathercalculations.weathercalculations.pressure_desc","Item that provides the local atmospheric pressure in mbar/hPa");

    public static final String CONFIG_HUMIDITY_ITEM = "humidityItem";
    public static final I18nConstant CONFIG_HUMIDITY_ITEM_LABEL = new I18nConstant("thing-type.config.weathercalculations.weathercalculations.humidity_label","Humidity Item");
    public static final I18nConstant CONFIG_HUMIDITY_ITEM_DESC = new I18nConstant("thing-type.config.weathercalculations.weathercalculations.humidity_desc","Item that provides the current relative humidity in %");

    public static final String CONFIG_WIND_SPEED_ITEM = "windSpeedItem";
    public static final I18nConstant CONFIG_WIND_SPEED_ITEM_LABEL = new I18nConstant("thing-type.config.weathercalculations.weathercalculations.wind_speed_label", "Wind Speed Item");
    public static final I18nConstant CONFIG_WIND_SPEED_ITEM_DESC = new I18nConstant("thing-type.config.weathercalculations.weathercalculations.wind_speed_desc","Item that provides the current wind speed in meters per second");


    private static TranslationProvider translationProvider;
    private static WeatherCalculationsHandlerFactory handlerFactory;

    public static void setHandlerFactory(WeatherCalculationsHandlerFactory handlerFactory) {
        WeatherCalculationsBindingConstants.handlerFactory = handlerFactory;
    }

    protected void setI18nProvider(TranslationProvider translationProvider) {
        WeatherCalculationsBindingConstants.translationProvider = translationProvider;
    }

    protected void unsetI18nProvider(TranslationProvider i18nProvider) {
        WeatherCalculationsBindingConstants.translationProvider = null;
    }

    public static String getI18nConstant(I18nConstant constant) {
        TranslationProvider translationProviderLocal = translationProvider;
        if (translationProviderLocal == null) {
            return MessageFormat.format(constant.defaultText, (Object[]) null);
        }
        return translationProviderLocal.getText(handlerFactory.getBundle(), constant.defaultText,
                constant.defaultText, null, (Object[]) null);
    }

    public static String getI18nConstant(I18nConstant constant, Object... arguments) {
        TranslationProvider translationProviderLocal = translationProvider;
        if (translationProviderLocal == null) {
            return MessageFormat.format(constant.key, arguments);
        }
        return translationProviderLocal.getText(handlerFactory.getBundle(), constant.key,
                constant.defaultText, null, arguments);
    }

    public static class I18nConstant {
        public String key;
        public String defaultText;

        I18nConstant(String key, String defaultText) {
            this.key = key;
            this.defaultText = defaultText;
        }
    }
}

